const express = require("express");

function createRouter(db) {
    const router = express.Router();

    router.get("/getAllUsers", function(req, res, next) {
        db.query( "SELECT idUser, nameUser, paternallastnameuser, maternallastnameuser, nicknameuser, genderuser FROM users", [],
            (error, results) => {
                if (error) {
                    res.status(500).json({status: "error"});
                } else {
                    res.status(200).json(results.rows);
                }
            }
        );
    });

    router.get("/login/user=:user&password=:password", function(req, res) {
        var username = req.params.user;
        var password = req.params.password;
        var query = `select * from users where nicknameuser = '${username}' and passworduser = '${password}' `
        if(username && password) {
            db.query( query, [],
                (error, results) => {
                    if(results.rows.length > 0) {
                        res.status(200).json({status: "success", data : results.rows});
                    } else {
                        res.status(500).json({status: "error"});
                    }
                }
            );
        } else {
            res.status(200).json({status: "error"});
        }
        
    });

    router.get("/selectUser/id=:id", function(req, res, next) {
        db.query( "SELECT idUser, nameUser, paternallastnameuser, maternallastnameuser, nicknameuser, genderuser, passwordUser FROM users WHERE idUser="+ req.params.id, [],
            (error, results) => {
                if (error) {
                        res.status(500).json({status: "error"});
                } else {
                        res.status(200).json({status: "success", data : results.rows});
                }
            }
        );
    });

    router.get("/updateUSer/id=:id&name=:name&p_lastname=:p_lastname&m_lastname=:m_lastname&nick=:nick&gender=:gender&password=:password", function(req, res, next) {
        var id = req.params.id;
        var nick = req.params.nick;
        var name = req.params.name;
        var p_lastname = req.params.p_lastname;
        var m_lastname = req.params.m_lastname;
        var gender = req.params.gender;
        var password = req.params.password;
        var query = `update users set nicknameuser = '${nick}',  nameuser = '${name}',  paternallastnameuser = '${p_lastname}',  maternallastnameuser = '${m_lastname}',  genderuser = '${gender}',  passworduser = '${password}' where iduser = ${id}`
        console.log(query)
        db.query(query, [],
            (error) => {
                if (error) {
                        res.status(500).json({status: "error"});
                } else {
                        res.status(200).json({status: "success"});
                }
            }
        );
    });

  return router;

}

module.exports = createRouter;