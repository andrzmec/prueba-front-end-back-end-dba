
const { Client } = require('pg');

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const events = require('./events.js');


const connectionData = {
    user: 'postgres',
    host: 'localhost',
    database: 'soluciones_integrales',
    password: '****',
    port: 5432,
}

const client = new Client(connectionData)

client.connect()


const port = process.env.PORT || 9090;
const app = express()
  .use(cors())
  .use(bodyParser.json())
  .use(events(client));
  
app.listen(port, () => {
  console.log(`Express server listening on port ${port}`);
});