create database soluciones_integrales

create table users (
	idUser serial primary key not null,
	nameUser varchar(30) not null,
	paternalLastnameUser varchar(30) not null,
	maternalLastnameUser varchar(30),
	nicknameUser varchar(20) not null,
	avatarUser text,
	passwordUser varchar(20) not null,
	genderUser varchar(20)
)

insert into users (nameUser, paternalLastnameUser, maternalLastnameUser, nicknameUser, passwordUser) 
	values('Octavio Andrés','Mendoza','Carrera','andrzmec','PlusUltra!123')
insert into users (nameUser, paternalLastnameUser, maternalLastnameUser, nicknameUser, passwordUser) 
	values('Homero','Simpson','Fox','homer','dooh!')
insert into users (nameUser, paternalLastnameUser, maternalLastnameUser, nicknameUser, passwordUser) 
	values('Marge','Simpson','Fox','marge','hmmm!')
insert into users (nameUser, paternalLastnameUser, maternalLastnameUser, nicknameUser, passwordUser) 
	values('Bart','Simpson','Fox','barto','AyCaramba!')
