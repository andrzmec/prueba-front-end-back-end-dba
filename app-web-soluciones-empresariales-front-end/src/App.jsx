import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import HomeComponent from './components/open/HomeComponent/HomeComponent'
import MainComponent from './components/secure/MainComponent/MainComponent'

function App() {

  const notFound = () => <Redirect to='/' />

  const PrivacyRoute = ({component, path, ...rest}) => {
    if (localStorage.getItem('app-web-user')){
      const userStorage = JSON.parse(localStorage.getItem('app-web-user'))
      if(userStorage.status === "success"){
        return <Route component={component} path={path} {...rest}/>
      } else {
        return <Redirect to="/" />
      }
    } else {
      return <Redirect to="/" />
    }
  }

  return (
    <Router>
      <Switch>
        <Route component={HomeComponent} path="/" exact/>
        <PrivacyRoute component={MainComponent} path="/main" exact/>
        <Route component={notFound}/>
      </Switch>
    </Router>
  );
}

export default App;
