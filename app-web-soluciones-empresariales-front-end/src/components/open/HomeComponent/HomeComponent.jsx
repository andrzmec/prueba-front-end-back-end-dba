import React, {useEffect} from 'react'
import './HomeComponent.css'
import {useSelector} from 'react-redux'
import SignComponent from '../../templates/SignComponent/SignComponent'
import WelcomeComponent from '../../templates/WelcomeComponent/WelcomeComponent'
import {withRouter} from 'react-router-dom'

const HomeComponent = (props) => {
    
    const active = useSelector(store => store.signin.active)

    useEffect(() => {
        if (active)  props.history.push('/main')
    }, [active, props.history])

    return active !== true ? (
        <section id="app-home-soluciones-empresariales">
            <WelcomeComponent></WelcomeComponent>
            <SignComponent></SignComponent>
        </section>
    ): null
}

export default withRouter(HomeComponent)
