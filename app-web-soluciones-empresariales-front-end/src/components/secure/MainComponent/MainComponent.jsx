import React, {useEffect} from 'react'
import './MainComponent.css'
import { useSelector } from 'react-redux'
import DatatableUserComponent from '../../templates/DatatableUsersComponent/DatatableUserComponent'
import MenuComponent from '../../templates/MenuComponent/MenuComponent'
import {withRouter} from 'react-router-dom'

const MainComponent = (props) => {
    
    const active = useSelector(store => store.signin.active)

    useEffect(() => {
        if (active === false)  props.history.push('/')
    }, [active, props.history])

    return active ? (
        <section id="app-main-soluciones-empresariales">
            <MenuComponent></MenuComponent>
            <DatatableUserComponent></DatatableUserComponent>
        </section>
    ): null
}

export default withRouter(MainComponent)
