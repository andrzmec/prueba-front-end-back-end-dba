import React, { useState, useEffect } from 'react'
import './DatatableUserComponent.css'
import { useDispatch, useSelector } from 'react-redux'
import DataTable from 'react-data-table-component'
import DetailsUserComponent from '../DetailsUserComponent/DetailsUserComponent';
import { getUserByIdAction, getUsersAction } from '../../../services/ducks/crudUserDuck';


const DatatableUserComponent = () => {

  const dispatch = useDispatch()

  const users = useSelector(store => store.crudUser.results)

  useEffect(() => {
    dispatch(getUsersAction())
  }, [dispatch])


  const [modalShow, setModalShow] = useState(false);

  const details = (x) => {
      setModalShow(true)
      dispatch(getUserByIdAction(x))
  }

  const button = (x) => <button className="details" onClick={() => details(x)}>Detalles</button>


  const columns = [
      {
        name: 'Nickname',
        selector: 'nicknameuser',
        sortable: true
      },
      {
        name: 'Nombres',
        selector: 'nameuser',
        sortable: true,
        right: true
      },
      {
        name: 'Apellido Paterno',
        selector: 'paternallastnameuser',
        sortable: true,
        right: true
      },
      {
        name: 'Apellido Materno',
        selector: 'maternallastnameuser',
        sortable: true,
        right: true
      },
      {
          name: 'Género',
          selector: 'genderuser',
          sortable: true,
          right: true
        },
      {
        name: 'Acciones',
        button: true,
        cell: row => button(row.iduser)
      }
  ]

  return (
      <>
          <div id="app-datatable-user-soluciones-integrales">
              <DataTable
                  title="Usuarios"
                  columns={columns}
                  data={users}
                  pagination={true}
              />
          </div>
          {
            modalShow && (<DetailsUserComponent show={modalShow} onHide={() => setModalShow(false)}></DetailsUserComponent>)
          }
          
      </>
  )
}

export default DatatableUserComponent
