import React, { useState } from 'react'
import './DetailsUserComponent.css'
import {  useSelector, useDispatch } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { getUsersAction, updateUserByIdAction } from '../../../services/ducks/crudUserDuck'

const DetailsUserComponent = (props) => {

    const dispatch = useDispatch()

    const userselected = useSelector(store => store.crudUser.selected)

    const [nickname, setnickname] = useState('')
    const [name, setname] = useState('')
    const [p_lastname, setp_lastname] = useState('')
    const [m_lastname, setm_lastname] = useState('')
    const [gender, setgender] = useState('')
    const [pass, setpass] = useState('')

    const updateForm = e => {
        e.preventDefault()

        let n1 = ''
        let n2 = ''
        let n3 = ''
        let n4 = ''
        let n5 = ''
        let n6 = ''

        if(nickname === '') {n1 = userselected.nicknameuser} else {n1 = nickname} 
        if(name === '') {n2 = userselected.nameuser} else {n2 = name} 
        if(p_lastname === '') {n3 = userselected.paternallastnameuser} else {n3 = p_lastname} 
        if(m_lastname === '') {n4 = userselected.maternallastnameuser} else {n4 = m_lastname} 
        if(gender === '') {n5 = userselected.genderuser} else {n5 = gender} 
        if(pass === '') {n6 = userselected.passworduser} else {n6 = pass} 
        
        dispatch(updateUserByIdAction(userselected.iduser, n1, n2, n3, n4, n5, n6))
        dispatch(getUsersAction())
        props.onHide()
    }

    return(
        <Modal className="details-modal" {...props} size="lg" aria-labelledby="DetailsModalComponent" centered>
            <Modal.Header closeButton>
                <Modal.Title id="DetailsModalComponent">
                    {userselected.nicknameuser}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {
                    <form onSubmit={updateForm}>
                        <div className="input">
                            <span>{userselected.nicknameuser}</span>
                            <input type="text" value={nickname} placeholder="cambiar nickname" onChange={ e => setnickname(e.target.value) }/>
                        </div>
                        <div className="input">
                            <span>{userselected.nameuser}</span>
                            <input type="text" value={name} placeholder="cambiar nombre(s)" onChange={ e => setname(e.target.value) }/>
                        </div>
                        <div className="input">
                            <span>{userselected.paternallastnameuser}</span>
                            <input type="text" value={p_lastname} placeholder="cambiar apellido paterno" onChange={ e => setp_lastname(e.target.value) }/>
                        </div>
                        <div className="input">
                            <span>{userselected.maternallastnameuser}</span>
                            <input type="text" value={m_lastname} placeholder="cambiar apellido materno" onChange={ e => setm_lastname(e.target.value) }/>
                        </div>
                        <div className="input">
                            <span>{userselected.genderuser}</span>
                            <input type="text" value={gender} placeholder="cambiar genero" onChange={ e => setgender(e.target.value) }/>
                        </div><div className="input">
                            <span>{userselected.passworduser}</span>
                            <input type="text" value={pass} placeholder="cambiar contraseña" onChange={ e => setpass(e.target.value) }/>
                        </div>
                        <button type="submit">Actualizar al usuario</button>
                    </form>
                }
                
            </Modal.Body>
        </Modal>
    )
}

export default DetailsUserComponent
