/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import './MenuComponent.css'
import 'fa-icons';
import { Dropdown } from 'react-bootstrap';
import { useDispatch } from 'react-redux'
import { logoutUserAction } from '../../../services/ducks/signinDuck'
import { withRouter } from 'react-router-dom'

const MenuComponent = (props) => {

    const dispatch = useDispatch()

    const logOut = () => {
        dispatch(logoutUserAction())
        props.history.push('/login')
    }

    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
        <a href="#" ref={ref} onClick={(e) => { e.preventDefault(); onClick(e); }} >
          {children}
        </a>
    ))

    return (
        <nav id="app-menu-soluciones-empresariales">
            <div className="activities">
                <ul className="activities-items">
                    <li className="activities-item">
                        <Dropdown alignRight>
                            <Dropdown.Toggle as={CustomToggle}  id="ewisemaps-user-actions">
                                <i className="far fa-user"></i>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <Dropdown.Item className="ewisemaps-user-item-actions" eventKey="logout" onClick={() => logOut()}>
                                    <i className="fas fa-sign-out-alt"></i> <span>Salir</span>
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default withRouter(MenuComponent)
