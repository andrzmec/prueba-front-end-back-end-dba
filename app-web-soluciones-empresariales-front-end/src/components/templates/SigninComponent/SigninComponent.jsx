import React, {useState} from 'react'
import './SigninComponent.css'
import { useDispatch } from 'react-redux'
import { getSigninAction } from '../../../services/ducks/signinDuck'

const SiginComponent = () => {

    const dispatch = useDispatch()
    const [user, setUser] = useState('')
    const [password, setPassword] = useState('')

    const signinForm = e => {
        e.preventDefault()
        if(!user.trim() || !password.trim())  return
        dispatch(getSigninAction(user, password))
    }


    return (
        <div id="app-sigin-soluciones-empresariales">
            <h3>Ingresa tu información para acceder a la plataforma...</h3>
            <form onSubmit={signinForm}>
                <input type="text" value={user} placeholder="usuario" onChange={ e => setUser(e.target.value) } required/>
                <input type="password" value={password} placeholder="contraseña" onChange={ e => setPassword(e.target.value) } required/>
                <button type="submit">Acceder</button>
            </form>
        </div>
    )
}

export default SiginComponent
