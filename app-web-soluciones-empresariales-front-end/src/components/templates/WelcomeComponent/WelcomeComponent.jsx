import React from 'react'
import './WelcomeComponent.css'
import 'fa-icons'

const WelcomeComponent = () => {
    return (
        <div id="app-welcome-soluciones-empresariales">
            <div className="logo">
                <i className="far fa-building"></i>
            </div>
            <div className="hello">
                <h1>Hola!!!</h1>
                <h4>Espero que te encuentres muy bien</h4>
            </div>
            <div className="info">
                <p>
                    Esta plataforma tiene como fin, el ayudar a Soluciones Empresariales S.A. de C.V.
                    a mejorar el proceso de publicar los proyectos semana con semana.
                </p>
                <p>
                    En esta versión se podrán gestionar los usuarios dentro de la plataforma, esto para
                    tener un control de los registros y evaluar su información.
                </p>
            </div>
        </div>
    )
}

export default WelcomeComponent
