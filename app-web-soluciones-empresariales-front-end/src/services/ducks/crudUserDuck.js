import axios from 'axios';


// consts
const initData = {
    results: [],
    selected: {
        genderuser: "",
        iduser: null,
        maternallastnameuser: "",
        nameuser: "",
        nicknameuser: "",
        passworduser: "",
        paternallastnameuser: ""
    }
}


// types
const GET_USER_SUCESS = 'GET_USER_SUCESS'
const UPDATE_USER_BY_ID_SUCESS = 'UPDATE_USER_BY_ID_SUCESS'
const GET_USER_ERROR = 'GET_USER_ERROR'
const GET_USER_BY_ID_SUCESS = 'GET_USER_BY_ID_SUCESS'
const GET_USER_BY_ID_ERROR = 'GET_USER_BY_ID_ERROR'


// reducers
export default function crudUSerReducer(state = initData, action){
    switch(action.type){
        case GET_USER_SUCESS:
            return {...state, results: action.payload}
        case GET_USER_ERROR:
            return {...initData}
        case GET_USER_BY_ID_SUCESS:
            return {...state, selected: action.payload}
        case GET_USER_BY_ID_ERROR:
            return {...initData}
        case UPDATE_USER_BY_ID_SUCESS:
            return {...initData}
        default:
            return state
    }
}


// actions
export const getUsersAction = ( ) =>  async(dispatch) => {
    try{
        let res =await ( await axios.get(`http://localhost:9090/getAllUsers`)).data
        dispatch({
            type: GET_USER_SUCESS,
            payload :  res
        })
    }catch(error){
        dispatch({
            type: GET_USER_ERROR
        })
    }
}

export const getUserByIdAction = ( id ) =>  async(dispatch) => {
    try{
        let res =await ( await axios.get(`http://localhost:9090/selectUser/id=${id}`)).data
        dispatch({
            type: GET_USER_BY_ID_SUCESS,
            payload :  res.data[0]
        })
    }catch(error){
        dispatch({
            type: GET_USER_BY_ID_ERROR
        })
    }
}

export const updateUserByIdAction = ( id, nick, name, pname, mname, gender, pass ) =>  async(dispatch) => {
    try{
        let res =await ( await axios.get(`http://localhost:9090/updateUSer/id=${id}&name=${name}&p_lastname=${pname}&m_lastname=${mname}&nick=${nick}&gender=${gender}&password=${pass}`)).data
        dispatch({
            type: UPDATE_USER_BY_ID_SUCESS,
            payload :  res.status
        })
    }catch(error){
        dispatch({
            type: GET_USER_BY_ID_ERROR
        })
    }
}


