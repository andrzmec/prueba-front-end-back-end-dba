import axios from 'axios';


// consts
const initData = {
    active: false
}


// types
const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS'
const SIGNIN_ERROR = 'SIGNIN_ERROR'
const USER_SIGNIN_SUCESS = 'USER_SIGNIN_SUCESS'
const SIGNOUT_SUCCESS = 'SIGNOUT_SUCCESS'

// reducers
export default function signinReducer(state = initData, action){
    switch(action.type){
        case SIGNIN_SUCCESS:
            return {...state, active: action.payload}
        case SIGNIN_ERROR:
            return {...initData}
        case USER_SIGNIN_SUCESS:
            return {...state, active: true, user: action.payload}
        case SIGNOUT_SUCCESS:
            return {...initData}
        default:
            return state
    }
}


// actions
export const getSigninAction = (user, password ) =>  async(dispatch) => {
    try{
        let res =await ( await axios.get(`http://localhost:9090/login/user=${user}&password=${password}`)).data
        dispatch({
            type: SIGNIN_SUCCESS,
            payload :  true
        })
        localStorage.setItem('app-web-user', JSON.stringify(res))
    }catch(error){
        dispatch({
            type: SIGNIN_ERROR
        })
    }
}

export const readActiveUSerAction = () => async (dispatch) => {
    if(localStorage.getItem('app-web-user')) {
        dispatch({
            type: USER_SIGNIN_SUCESS,
            payload: JSON.parse(localStorage.getItem('app-web-user'))
        })
    }
}

export const logoutUserAction = () => (dispatch) => {
    localStorage.removeItem('app-web-user')
    dispatch({
        type: SIGNOUT_SUCCESS
    })
}

