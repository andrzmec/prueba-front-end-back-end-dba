import {createStore, combineReducers, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

import signinReducer, {readActiveUSerAction} from '../ducks/signinDuck'
import crudUserReducer from '../ducks/crudUserDuck'

const rootReducer = combineReducers({
    signin: signinReducer,
    crudUser: crudUserReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore() {
    const store = createStore( rootReducer, composeEnhancers( applyMiddleware(thunk) ) )
    readActiveUSerAction()(store.dispatch)
    return store
}